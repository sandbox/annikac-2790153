<?php

namespace Drupal\entity_print_form\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\entity_print\PrintBuilderInterface;
use Drupal\entity_print\PrintEngineException;
use Drupal\entity_print\Plugin\ExportTypeManagerInterface;
use Drupal\entity_print\Plugin\EntityPrintPluginManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * Controller class for printing Forms.
 */
class ViewFormController extends ControllerBase {

  /**
   * The plugin manager for our Print engines.
   *
   * @var \Drupal\entity_print\Plugin\EntityPrintPluginManager
   */
  protected $pluginManager;

  /**
   * The export type manager.
   *
   * @var \Drupal\entity_print\Plugin\ExportTypeManagerInterface
   */
  protected $exportTypeManager;

  /**
   * The Print builder.
   *
   * @var \Drupal\entity_print\PrintBuilderInterface
   */
  protected $printBuilder;

  /**
   * The Entity Type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The Entity manager.
   *
   * @var \Drupal\Core\Entity\EntityManagerInterface
   */
  protected $entityManager;

  /**
   * The Form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $currentRequest;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityPrintPluginManagerInterface $plugin_manager, ExportTypeManagerInterface $export_type_manager, PrintBuilderInterface $print_builder, EntityTypeManagerInterface $entity_type_manager, EntityManagerInterface $entity_manager, FormBuilderInterface $form_builder, Request $current_request, AccountInterface $current_user) {
    $this->pluginManager = $plugin_manager;
    $this->exportTypeManager = $export_type_manager;
    $this->printBuilder = $print_builder;
    $this->entityTypeManager = $entity_type_manager;
    $this->entityManager = $entity_manager;
    $this->formBuilder = $form_builder;
    $this->currentRequest = $current_request;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.entity_print.print_engine'),
      $container->get('plugin.manager.entity_print.export_type'),
      $container->get('entity_print_form.form_print_builder'),
      $container->get('entity_type.manager'),
      $container->get('entity.manager'),
      $container->get('form_builder'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('current_user')
    );
  }

  /**
   * Print an entity to the selected format.
   *
   * @param string $export_type
   *   The export type.
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_id
   *   The entity id.
   * @param string $form_mode
   *   The form mode to render.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object on error otherwise the Print is sent.
   */
  public function viewForm($export_type, $entity_type, $entity_id, $form_mode) {
    // Create the Print engine plugin.
    $config = $this->config('entity_print.settings');
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    $form_object = $this->entityManager->getFormObject($entity->getEntityTypeId(), $form_mode);
    $form_object->setEntity($entity);

    try {
      $print_engine = $this->pluginManager->createSelectedInstance($export_type);
    }
    catch (PrintEngineException $e) {
      // Build a safe markup string using Xss::filter() so that the instructions
      // for installing dependencies can contain quotes.
      drupal_set_message(new FormattableMarkup('Error generating Print: ' . Xss::filter($e->getMessage()), []), 'error');

      $url = Url::fromUserInput('<front>');
      return new RedirectResponse($url);
    }

    return (new StreamedResponse(function() use ($form_object, $print_engine, $config) {
      // The printed document is sent straight to the browser.
      $this->printBuilder->deliverPrintable([$form_object], $print_engine, $config->get('force_download'), $config->get('default_css'));
    }))->send();
  }

  /**
   * A debug callback for styling up the Form Print.
   *
   * @param string $entity_type
   *   The entity type.
   * @param int $entity_id
   *   The entity id.
   * @param string $form_mode
   *   The form mode.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response object.
   *
   * @TODO, improve permissions in https://www.drupal.org/node/2759553
   */
  public function viewFormPrintDebug($entity_type, $entity_id, $form_mode) {
    $entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id);
    $form_object = $this->entityManager->getFormObject($entity->getEntityTypeId(), $form_mode);
    $form_object->setEntity($entity);
    try {
      $use_default_css = $this->config('entity_print.settings')->get('default_css');
      return new Response($this->printBuilder->printFormHtml($form_object, $use_default_css, FALSE));
    }
    catch (PrintEngineException $e) {
      drupal_set_message(new FormattableMarkup('Error generating Print: ' . Xss::filter($e->getMessage()), []), 'error');
      return new RedirectResponse($entity->toUrl()->toString());
    }
  }

  /**
   * Validate that the current user has access.
   *
   * We need to validate that the user is allowed to access this entity also the
   * print version.
   *
   * @param string $export_type
   *   The export type.
   * @param string $entity_type
   *   The entity type.
   * @param string $entity_id
   *   The entity id
   * @param string $form_mode
   *   The form mode to render
   *
   * @return \Drupal\Core\Access\AccessResult
   *   The access result object.
   */
  public function checkAccess($export_type, $entity_type, $entity_id, $form_mode) {
    if (empty($entity_id)) {
      return AccessResult::forbidden();
    }

    $account = $this->currentUser();

    // Invalid storage type.
    if (!$this->entityTypeManager->hasHandler($entity_type, 'storage')) {
      return AccessResult::forbidden();
    }

    // Unable to find the entity requested.
    if (!$entity = $this->entityTypeManager->getStorage($entity_type)->load($entity_id)) {
      return AccessResult::forbidden();
    }

    // Ensure it's a valid export type.
    if (!in_array($export_type, array_keys($this->exportTypeManager->getDefinitions()))) {
      return AccessResult::forbidden();
    }

    // Check if the user has the permission "bypass entity print access".
    $access_result = AccessResult::allowedIfHasPermission($account, 'bypass entity print access');
    if ($access_result->isAllowed()) {
      return $access_result->andIf($entity->access('view', $account, TRUE));
    }

    // Check if the user is allowed to view all bundles of the entity type.
    $access_result = AccessResult::allowedIfHasPermission($account, 'entity print access type ' . $entity_type);
    if ($access_result->isAllowed()) {
      return $access_result->andIf($entity->access('view', $account, TRUE));
    }

    // Check if the user is allowed to view that bundle type.
    $access_result = AccessResult::allowedIfHasPermission($account, 'entity print access bundle ' . $entity->bundle());
    if ($access_result->isAllowed()) {
      return $access_result->andIf($entity->access('view', $account, TRUE));
    }

    return AccessResult::forbidden();
  }

}
