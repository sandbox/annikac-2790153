<?php

namespace Drupal\entity_print_form\Renderer;

use Drupal\Core\Asset\AssetCollectionRendererInterface;
use Drupal\Core\Asset\AssetResolverInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\InfoParserInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormState;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;


class EntityFormRenderer extends FormRendererBase implements FormRendererInterface {

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  public function __construct(ThemeHandlerInterface $theme_handler, InfoParserInterface $info_parser, AssetResolverInterface $asset_resolver, AssetCollectionRendererInterface $css_renderer, RendererInterface $renderer, EventDispatcherInterface $event_dispatcher, FormBuilderInterface $form_builder, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($theme_handler, $info_parser, $asset_resolver, $css_renderer, $renderer, $event_dispatcher, $form_builder);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function render(EntityFormInterface $form_object) {
    $form_state = (new FormState());
    $built_form = $this->formBuilder->buildForm($form_object, $form_state);
    return $built_form;
  }

  /**
   * {@inheritdoc}
   */
  public function getFilename(array $entities) {
    $filenames = [];
    foreach ($entities as $entity) {
      $filenames[] = $this->sanitizeFilename($entity->getEntity()->label());
    }
    return implode('-', $filenames);
  }

}
