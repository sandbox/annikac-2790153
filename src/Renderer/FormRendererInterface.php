<?php

namespace Drupal\entity_print_form\Renderer;

use Drupal\Core\Entity\EntityFormInterface;

interface FormRendererInterface {

  /**
   * Gets the renderable for this entity form.
   *
   * @param \Drupal\Core\Entity\EntityFormInterface $form_object
   *   The entity we're rendering.
   *
   * @return array
   *   The renderable array for the entity form.
   */
  public function render(EntityFormInterface $form_object);

  /**
   * Generates the HTML from the renderable array of entities.
   *
   * @param array $entities
   *   An array of entities we're rendering.
   * @param array $render
   *   A renderable array.
   * @param bool $use_default_css
   *   TRUE if we should inject our default CSS otherwise FALSE.
   * @param bool $optimize_css
   *   TRUE if we should compress the CSS otherwise FALSE.
   * @return mixed
   */
  public function generateHtml(array $entities, array $render, $use_default_css, $optimize_css);

  /**
   * Get the filename for the entity we're printing *without* the extension.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities for which to generate the filename from.
   * @return string
   *   The generate file name for this entity.
   */
  public function getFilename(array $entities);

}
