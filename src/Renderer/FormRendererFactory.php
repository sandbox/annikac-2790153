<?php

namespace Drupal\entity_print_form\Renderer;

use Drupal\Core\Entity\EntityFormInterface;
use Drupal\entity_print\PrintEngineException;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

use Drupal\entity_print\Renderer\RendererFactoryInterface;

/**
 * The FormRendererFactory class.
 */
class FormRendererFactory implements RendererFactoryInterface {

  use ContainerAwareTrait;

  /**
   * {@inheritdoc}
   */
  public function create($item, $context = 'form') {
    // If we get an array or something, just look at the first one.
    if (is_array($item)) {
      $item = array_pop($item);
    }
    if (!empty($item)) {
      if ($this->container->has("entity_print_form.renderer.form")) {
        return $this->container->get("entity_print_form.renderer.form");
      }
    }

    throw new PrintEngineException(sprintf('Rendering not yet supported for "%s". Entity Print context "%s"', is_object($item) ? get_class($item) : $item, $context));
  }

}
